package com.demo.crud;

import java.util.Objects;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.demo.crud.persistence.model.Product;
import com.demo.crud.persistence.model.User;
import com.demo.crud.persistence.repository.IProductRepository;
import com.demo.crud.persistence.repository.IUserRepository;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "CRUD example API", version = "1.0", description = "GUI to test exposed endpoints :)"))
public class CrudExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudExampleApplication.class, args);
	}

	@Bean
	public CommandLineRunner initialLoad(IUserRepository repository) {
		return (args) -> {

			if (Objects.isNull(repository.findByName("youssef")))
				repository.save(new User("youssef", "youssef@gmail.com", "USER"));
			if (Objects.isNull(repository.findByName("liliane")))
				repository.save(new User("liliane", "liliane@gmail.com", "ADMIN"));
			if (Objects.isNull(repository.findByName("zaynab")))
				repository.save(new User("zaynab", "zaynab@gmail.com", "ADMIN"));
			if (Objects.isNull(repository.findByName("rabih")))
				repository.save(new User("rabih", "rabih@gmail.com", "USER"));
		};
	}

	@Bean
	public CommandLineRunner loadProduct(IProductRepository repository) {
		return (args) -> {
			if (Objects.isNull(repository.findByName("shirt")))
				repository.save(new Product("Gucci", "shirt", "100"));
			if (Objects.isNull(repository.findByName("pants")))
				repository.save(new Product("DIOR", "pants", "200"));
			if (Objects.isNull(repository.findByName("shoes")))
				repository.save(new Product("Nike", "shoes", "300"));
			if (Objects.isNull(repository.findByName("slippers")))
				repository.save(new Product("Adidas", "slippers", "400"));
		};
	}
}
