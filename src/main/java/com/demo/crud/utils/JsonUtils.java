package com.demo.crud.utils;

import org.json.JSONObject;

public class JsonUtils {
	
	public static String toJsonString (Object obj) {
		return (new JSONObject(obj)).toString();
	}

}