package com.demo.crud.persistence.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.demo.crud.persistence.model.UserCredentials;

public interface IUserCredntialsRepository extends CrudRepository<UserCredentials, Long>{
	
	List<UserCredentials> findByUsername(String username);

}