package com.demo.crud.persistence.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.crud.persistence.model.User;

@Repository
public interface IUserRepository extends CrudRepository<User, Long>{
    
	// additional finder methods to query our DB
	// Notice the naming convention: the methods are automatically implemented by JPA
	
    List<User> findByName(String name);
    List<User> findByRole(String role);
    List<User> findByEmail(String email);
    
}
