package com.demo.crud.persistence.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.crud.persistence.model.Product;

@Repository
public interface IProductRepository extends CrudRepository<Product, Long> {
	
	List<Product> findByName(String name);
	List<Product> findByBrand(String brand);
	List<Product> findByPrice(String price);
	
}
