package com.demo.crud.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.demo.crud.utils.JsonUtils;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
public class User {
	
	
	public User(String name, String email, String role) {
		this.name = name;
		this.email = email;
		this.role = role;
	}

	@Id
	@SequenceGenerator(name = "USER_ID", sequenceName = "USER_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="USER_ID")
	@Column(name = "id")
	private long id;
	
	@Column(name = "name")
	@NotBlank(message = "Name cannot be empty!")
	private String name;

	@Column(name = "email", unique = true)
	@NotBlank(message = "Email cannot be empty!")
	@Pattern(regexp = "[A-Za-z0-9._-]{2,}@[A-Za-z0-9]{2,}\\.[A-Za-z]{1,}(?:\\.[A-Za-z]{1,})?", message = "Enter a Valid email address!")
	private String email;
	
	@Column(name = "role")
	@NotBlank(message = "Role cannot be empty!")
	private String role;
	
//	@OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL, mappedBy = "user")
//	private UserCredentials userCredentials;
	
	@Override
	public String toString() {
		return JsonUtils.toJsonString(this);
	}
}