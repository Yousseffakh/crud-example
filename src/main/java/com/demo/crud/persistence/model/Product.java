package com.demo.crud.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.demo.crud.utils.JsonUtils;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="products")
@Data
@NoArgsConstructor
public class Product {
	
	public Product(String brand, String name, String price) {
		this.brand=brand;
		this.name=name;
		this.price=price;
	}

	@Id
	@SequenceGenerator(name = "PRODUCT_ID", sequenceName = "PRODUCT_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="PRODUCT_ID")
	@Column(name="prod_id")
	private long id;

	@Column(name="prod_brand")
	@NotBlank(message="Brand cannot be empty!")
	private String brand;
	
	@Column(name="prod_name")
	@NotBlank(message="Name cannot be empty!")
	private String name;
	
	@Column(name="prod_price")
	private String price;
	
	@Override
	public String toString() {
		return JsonUtils.toJsonString(this);
	}
	
}
