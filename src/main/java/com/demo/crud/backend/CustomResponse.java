package com.demo.crud.backend;

import com.demo.crud.utils.JsonUtils;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@Hidden
public class CustomResponse {
	
	private String code;
	private String status;
	private String description;
	
	@Override
	public String toString() {
		return JsonUtils.toJsonString(this);
	}
}
