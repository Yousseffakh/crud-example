package com.demo.crud.web.security;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests(
        		request -> request
	        		.antMatchers(HttpMethod.POST, "/api/**").hasAuthority("ADMIN")
	        		.antMatchers(HttpMethod.PUT, "/api/**").hasAuthority("ADMIN")
	        		.antMatchers(HttpMethod.DELETE, "/api/**").hasAuthority("ADMIN")
	        		.antMatchers(HttpMethod.GET, "/swagger/**").hasAuthority("ADMIN")
	        		.antMatchers(HttpMethod.GET, "/h2db/**").hasAuthority("ADMIN")
        			.antMatchers(
        					"/",
        					"/index",
        					"/signup",
        					"/addUser",
        					"/edit/{code:^[0-9]*$}",
        					"/update/{code:^[0-9]*$}",
        					"/delete/{code:^[0-9]*$}"
        					)
        				.permitAll()
        			.antMatchers(HttpMethod.GET, "/api/**")
        				.permitAll()
        		)
            .httpBasic()
            .and()
            .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(passwordEncoder().encode("admin"))
                .authorities("ADMIN")
                .and()
                .withUser("user")
                .password(passwordEncoder().encode("user"))
                .authorities("USER");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
