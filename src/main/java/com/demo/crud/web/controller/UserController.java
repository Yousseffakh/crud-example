package com.demo.crud.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.demo.crud.persistence.model.User;
import com.demo.crud.persistence.repository.IUserRepository;

@Controller
@RequestMapping("valoores/users")
public class UserController {

	private final IUserRepository userRepository;

	@Autowired
	public UserController(IUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@GetMapping
	public String showUserList(Model model) {
		model.addAttribute("users", userRepository.findAll());
		return "user"; // user.html
	}

	@GetMapping("/add")
	public String showSignUpForm(User user) {
		return "add-user"; // add-user.html
	}

	@PostMapping("/create")
	public String addUser(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-user";
		}

		userRepository.save(user);
		return "redirect:/valoores/users";
	}

	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {

		User user = userRepository.findById(id).orElse(new User());

		if (user.getId() > 0) {
			model.addAttribute("user", user);
			return "update-user";
		}
		return "redirect:/valoores/users";
	}

	@PostMapping("/update/{id}")
	public String updateUser(@PathVariable("id") long id, @Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			user.setId(id);
			return "update-user";
		}

		userRepository.save(user);

		return "redirect:/valoores/users";
	}

	@GetMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") long id, Model model) {
		User user = userRepository.findById(id).orElse(new User());

		if (user.getId() > 0)
			userRepository.delete(user);

		return "redirect:/valoores/users";
	}

}