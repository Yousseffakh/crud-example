package com.demo.crud.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.demo.crud.persistence.model.Product;
import com.demo.crud.persistence.repository.IProductRepository;

@Controller
@RequestMapping("valoores/products")
public class ProductController {

	private IProductRepository productRepository;

	@Autowired
	public ProductController(IProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@GetMapping
	public String showAllProducts(Model model) {
		model.addAttribute("products", productRepository.findAll());
			return "product";
	}

	@GetMapping("/add")
	public String showAddForm(Product product) {
		return "add-product";
	}

	@PostMapping("/create")
	public String addProduct(@Valid Product product, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-product";
		}

		productRepository.save(product);
		return "redirect:/valoores/products";
	}

	@GetMapping("/edit/{id}")
	public String showEditForm(@PathVariable("id") long id, Model model) {
		Product product = productRepository.findById(id).orElse(new Product());
		if (product.getId() > 0){
			model.addAttribute("product", product);
			return "update-product";
		}

		return "redirect:/valoores/products";
	}

	@PostMapping("/update/{id}")
	public String editProduct(@PathVariable("id") long id, @Valid Product product, BindingResult result) {

		if (result.hasErrors()) {
			product.setId(id);
			return "update-product";
		}

		productRepository.save(product);
		return "redirect:/valoores/products";
	}

	@GetMapping("/delete/{id}")
	public String deleteProduct(@PathVariable("id") long id, Model model) {
		Product product = productRepository.findById(id).orElse(new Product());
		if (product.getId() > 0)
			productRepository.delete(product);
		return "redirect:/valoores/products";
	}

}
