package com.demo.crud.web.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.crud.backend.CustomResponse;
import com.demo.crud.persistence.model.User;
import com.demo.crud.persistence.repository.IUserRepository;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Users", description = "Users' exposed APIs")
@RestController
@RequestMapping("/api")
public class UserRestController {

	private final IUserRepository userRepository;

	@Autowired
	public UserRestController(IUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Operation(summary = "Get all users.")
	@GetMapping("/users")
	public Iterable<User> getUsers() {
		return userRepository.findAll();
	}

	@Operation(summary = "Get user provided an ID.")
	@GetMapping("/user/{id}")
	public User getUserById(
			@Parameter(description = "Provide the ID of the user you want to get.") @PathVariable("id") long id) {
		return userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
	}
	
	@Operation(summary = "Add a new user.")
	@PostMapping("/user/add")
	public CustomResponse addUser(
			@Valid @RequestBody User user,
			BindingResult results) {

		CustomResponse resp = CustomResponse.builder().build();

		if (results.hasErrors()) {
			resp.setCode("1");
			resp.setStatus("Fail");
			resp.setDescription("Invalid attribute " + results.getFieldError().getField() + ": rejected value ["
					+ results.getFieldError().getRejectedValue() + "].");
			return resp;
		} else {

			User usr = userRepository.save(user);

			if (userRepository.existsById(usr.getId())) {
				resp.setCode("0");
				resp.setStatus("Success");
				resp.setDescription("New user with id = " + user.getId() + " created.");
			} else {
				resp.setCode("1");
				resp.setStatus("Fail");
				resp.setDescription("User creation failed.");
			}

			return resp;
		}
	}

	@Operation(summary = "Update user information provided an ID.")
	@PutMapping("/user/update/{id}")
	public CustomResponse updateUser(
			@Parameter(description = "Provide the ID of the user you want to update.") @PathVariable("id") long id,
			@Valid @RequestBody User user,
			BindingResult results)
			{

		CustomResponse resp = CustomResponse.builder().build();

		if (results.hasErrors()) {
			resp.setCode("1");
			resp.setStatus("Fail");
			resp.setDescription("Invalid attribute " + results.getFieldError().getField() + ": rejected value ["
					+ results.getFieldError().getRejectedValue() + "].");
			return resp;
		} else {

			if (userRepository.existsById(id)) {

				User usr = userRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

				usr.setId(id);
				usr.setEmail(user.getEmail());
				usr.setName(user.getName());
				usr.setRole(user.getRole());

				userRepository.save(usr);

				resp.setCode("0");
				resp.setStatus("Success");
				resp.setDescription("New user with id = " + usr.getId() + " updated.");
			} else {
				resp.setCode("1");
				resp.setStatus("Fail");
				resp.setDescription("User with id = " + id + " cannot be found.");
			}

			return resp;
		}
	}

	@Operation(summary = "Delete user provided an ID.")
	@DeleteMapping("/user/delete/{id}")
	public CustomResponse deleteUserById(
			@Parameter(description = "Provide the ID of the user you want to delete.") @PathVariable("id") long id) {

		CustomResponse resp = CustomResponse.builder().build();

		if (userRepository.existsById(id)) {
			userRepository.deleteById(id);
			if (userRepository.existsById(id)) {
				resp.setCode("1");
				resp.setStatus("Fail");
				resp.setDescription("Error deleting user with id = " + id + ".");
			} else {
				resp.setCode("0");
				resp.setStatus("Success");
				resp.setDescription("User with id = " + id + " deleted.");
			}
		} else {
			resp.setCode("1");
			resp.setStatus("Failed");
			resp.setDescription("User with id = " + id + " not found.");
		}

		return resp;
	}

	@Operation(summary = "Filter users by name.")
	@GetMapping("/users/name/{name}")
	public List<User> getUserByName(
			@Parameter(description = "Provide the NAME you want to search for.") @PathVariable("name") String name) {
		return userRepository.findByName(name);
	}
	
	@Operation(summary = "Filter users by role.")
	@GetMapping("/users/role/{role}")
	public List<User> getUserByRole(
			@Parameter(description = "Provide the ROLE you want to search for.") @PathVariable("role") String role) {
		return userRepository.findByRole(role);
	}
	
	@Operation(summary = "Filter users by email.")
	@GetMapping("/users/email/{email}")
	public List<User> getUserByEmail(
			@Parameter(description = "Provide the EMAIL you want to search for.") @PathVariable("email") String email) {
		return userRepository.findByEmail(email);
	}

}
