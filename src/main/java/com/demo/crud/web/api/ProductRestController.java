package com.demo.crud.web.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import com.demo.crud.backend.CustomResponse;
import com.demo.crud.persistence.model.Product;
import com.demo.crud.persistence.repository.IProductRepository;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Products", description = "Products exposed APIs")
@RestController
@RequestMapping("/api")
public class ProductRestController {

	private IProductRepository productRepository;

	@Autowired
	public ProductRestController(IProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Operation(summary = "Get all products")
	@GetMapping("/products")
	public Iterable<Product> getProducts() {
		return productRepository.findAll();
	}

	@Operation(summary = "Get product provided an ID")
	@GetMapping("/product/{id}")
	public Product getProductById(
			@Parameter(description = "Provide the ID of the product you want to get:") @PathVariable("id") long id) {
		return productRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid product Id:" + id));
	}

	@Operation(summary = "Filter products by name")
	@GetMapping("/products/name/{name}")
	public List<Product> getProductByName(
			@Parameter(description = "Provide the name of the product you want to get:") @PathVariable("name") String name) {
		return productRepository.findByName(name);
	}

	@Operation(summary = "Filter products by brand")
	@GetMapping("/products/brand/{brand}")
	public List<Product> getProductByBrand(
			@Parameter(description = "Provide the brand of the product you want to get:") @PathVariable("brand") String brand) {
		return productRepository.findByBrand(brand);
	}

	@Operation(summary = "Filter products by price")
	@GetMapping("/products/price/{price}")
	public List<Product> getProductByPrice(
			@Parameter(description = "Provide the price of the product you want to get:") @PathVariable("price") String price) {
		return productRepository.findByPrice(price);
	}
	
	@Operation(summary = "Update product information provided an ID")
	@PutMapping("/product/update/{id}")
	public CustomResponse editProduct(
			@Parameter(description = "Provide the id of the product you want to update:") @PathVariable("id") long id,
			@Valid @RequestBody Product product, BindingResult results) {

		CustomResponse resp = CustomResponse.builder().build();

		if (results.hasErrors()) {
			
			resp.setCode("1");
			resp.setStatus("Fail");
			resp.setDescription("Invalid attribute " + results.getFieldError().getField() + " : rejected value ["
					+ results.getFieldError().getRejectedValue() + "]");
			return resp;

		} else {
			
			if (productRepository.existsById(id)) {
				Product prod = productRepository.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("Invalid ID: " + id));
				
				prod.setId(id);
				prod.setName(product.getName());
				prod.setBrand(product.getBrand());
				prod.setPrice(product.getPrice());
				
				productRepository.save(prod);

				resp.setCode("0");
				resp.setStatus("success");
				resp.setDescription("the product updated with id : " + prod.getId());

			} else {
				resp.setCode("1");
				resp.setStatus("Fail");
				resp.setDescription("Product with id " + id + " is not found");
			}

		}
		return resp;
	}

	@Operation(summary = "add a new product")
	@PostMapping("/product/add")
	public CustomResponse addProduct(@Valid @RequestBody Product product, BindingResult results) {
		CustomResponse resp = CustomResponse.builder().build();
		if (results.hasErrors()) {
			resp.setCode("1");
			resp.setStatus("Fail");
			resp.setDescription("Invalid attribute " + results.getFieldError().getField() + " the value rejected is: "
					+ results.getFieldError().getRejectedValue());
			return resp;
		} else {
			productRepository.save(product);
			if (productRepository.existsById(product.getId())) {
				resp.setCode("0");
				resp.setStatus("Success");
				resp.setDescription("this product with id: " + product.getId() + " added.");
			} else {
				resp.setCode("1");
				resp.setStatus("Fail");
				resp.setDescription("this product not added");
			}
			return resp;

		}
	}

	@Operation(summary = "delete product provided an ID")
	@DeleteMapping("/product/delete/{id}")
	public CustomResponse deleteProduct(
			@Parameter(description = "provide the ID of the product ypu want to delete:") @PathVariable("id") long id) {
		CustomResponse resp = CustomResponse.builder().build();
		if (productRepository.existsById(id)) {
			productRepository.deleteById(id);
			resp.setCode("0");
			resp.setStatus("Success");
			resp.setDescription("the product of id: " + id + " deleted");
		} else {
			resp.setCode("1");
			resp.setStatus("Fail");
			resp.setDescription("the product with id: " + id + " not found");
		}
		return resp;
	}
}
